#!/usr/bin/env ruby

require 'nokogiri'
require 'open-uri'
require 'uri'

class Choice
  def initialize(text)
    @text = text
    @score = 0
    @answer = 0
  end
  attr_accessor :text
  attr_accessor :score
  attr_accessor :answer
end

print "\nEnter question: "
question = gets.strip
print 'Enter the number of answer choices: '
choicesLength = gets.strip.to_i
choices = []
for i in 1..choicesLength
  print 'Enter answer choice #' + i.to_s + ': '
  choices.push(Choice.new(gets.strip))
end

puts "\nSearching..."
resultsTexts = []
searchResultsPage = Nokogiri::HTML(open('https://google.com/search?q=' + URI.encode(question.to_s)))
gotAnswer = false

instantAnswer = searchResultsPage.css('.mrH1y')
if instantAnswer.length > 0
  answer = instantAnswer.first.inner_text
  puts 'Found Google instant answer: "' + answer + "\""
  for choice in choices
    if choice.text.downcase.include? answer.downcase or answer.downcase.include? choice.text.downcase
      choice.answer = 1
    end
  end

  choices.sort_by!{|x| x.answer}.reverse!

  if choices.sum(&:answer) == 1
    puts "\nResult:\n" + choices[0].text + ' (based on Google instant answer)'
    gotAnswer = true
  else
    puts 'Google instant answer matched multiple or no answer choices.'
  end
end

if !gotAnswer
  results = searchResultsPage.css('.r a:first-of-type')
  score = 0
  threads = results.map do |i|
    Thread.new(i) do |i|
      if result = results.pop
        begin
          resultsPage = Nokogiri::HTML(open('http://google.com/' + result['href']))
          resultsTexts.push(resultsPage.at('body').inner_text)
          print '.'
        rescue
        end
      end
    end
  end
  threads.each {|t| t.join}
  puts "\n"

  for choice in choices
    for resultsText in resultsTexts
      choice.score += resultsText.scan(/(?=#{choice.text})/i).count
    end
  end

  choices.sort_by!{|x| x.score}.reverse!

  if choices.sum(&:score) > 0
    puts "\nPotential results:\n"
    for i in 0...choicesLength
      puts choices[i].text + ' (' + ((choices[i].score.to_f / choices.sum(&:score) * 10000).round / 100.0).to_s + '% probability)'
    end
  else
    puts 'None of the answer choices could be found.'
  end
end

puts "\n"
