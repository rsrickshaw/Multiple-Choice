# Multiple-Choice
This script works by getting the Google search results for the question.

If the script detects a Google "instant answer" that matches one of the answer choices, it will choose that answer.

![](images/instant-answer-demo.png)

![](images/instant-answer-cli.png)

If there is no "instant answer" available, the script will open all of the links from the first page of search results.  It  then counts the number of occurrences of each answer choice per page.  It assigns each choice a "score" based on the total number of occurrences across all tested pages.  It then calculates a probability to each choice which is equal to the individual choice's score divided by the sum of all the choices' scores.

![](images/fallback-demo.png)

![](images/fallback-cli.png)
